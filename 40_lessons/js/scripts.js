$(document).ready(function(){

	window.onload = init;
	var context;
	var bufferLoader;

	function init() {
	  window.AudioContext = window.AudioContext || window.webkitAudioContext;
	  context = new AudioContext();
	  bufferLoader = new BufferLoader(
	    context,
	    [
	      'audio/'+$("div p:nth-child(1)").data('audio'),
	    ],
	    finishedLoading
	    );
	  bufferLoader.load();
	}

	function finishedLoading(bufferList) {
		var audioSource = context.createBufferSource();
		audioSource.buffer = bufferList[0];
		audioSource.connect(context.destination);

		
		$("div p:nth-child(1)").textillate({
			initialDelay: $(this).data('initial-delay'),
			in: {
				delay: $('div p:nth-child(1)').data('delay'),
				effect: $('div p:nth-child(1)').data('effect'),
				shuffle: $('div p:nth-child(1)').data('shuffle'),
				sync: $('div p:nth-child(1)').data('sync'),
				reverse: $('div p:nth-child(1)').data('reverse'),
			},
		});
		
		$('div p:nth-child(1)').on('inAnimationBegin.tlt', function () {
			
			switch ($('div p:nth-child(1)').data('ambi')) {

				case 10 :
					$('div p:nth-child(1)').css({
						'visibility':'visible',
						'-webkit-animation': 'cssAnimationJ '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-moz-animation': 'cssAnimationJ '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-o-animation': 'cssAnimationJ '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-webkit-animation-fill-mode': 'forwards',
						'-moz-animation-fill-mode': 'forwards',
						'-o-animation-fill-mode': 'forwards'
					});
					break;
			
				case 9 :
					$('div p:nth-child(1)').css({
						'visibility':'visible',
						'-webkit-animation': 'cssAnimationI '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-moz-animation': 'cssAnimationI '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-o-animation': 'cssAnimationI '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-webkit-animation-fill-mode': 'forwards',
						'-moz-animation-fill-mode': 'forwards',
						'-o-animation-fill-mode': 'forwards'
					});
					break;

				case 8 :
					$('div p:nth-child(1)').css({
						'visibility':'visible',
						'-webkit-animation': 'cssAnimationH '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-moz-animation': 'cssAnimationH '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-o-animation': 'cssAnimationH '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-webkit-animation-fill-mode': 'forwards',
						'-moz-animation-fill-mode': 'forwards',
						'-o-animation-fill-mode': 'forwards'
					});
					break;

				case 7 :
					$('div p:nth-child(1)').css({
						'visibility':'visible',
						'-webkit-animation': 'cssAnimationG '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-moz-animation': 'cssAnimationG '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-o-animation': 'cssAnimationG '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-webkit-animation-fill-mode': 'forwards',
						'-moz-animation-fill-mode': 'forwards',
						'-o-animation-fill-mode': 'forwards'
					});
					break;

				case 6 :
					$('div p:nth-child(1)').css({
						'visibility':'visible',
						'-webkit-animation': 'cssAnimationF '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-moz-animation': 'cssAnimationF '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-o-animation': 'cssAnimationF '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-webkit-animation-fill-mode': 'forwards',
						'-moz-animation-fill-mode': 'forwards',
						'-o-animation-fill-mode': 'forwards'
					});
					break;

				case 5 :
					$('div p:nth-child(1)').css({
						'visibility':'visible',
						'-webkit-animation': 'cssAnimationE '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-moz-animation': 'cssAnimationE '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-o-animation': 'cssAnimationE '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-webkit-animation-fill-mode': 'forwards',
						'-moz-animation-fill-mode': 'forwards',
						'-o-animation-fill-mode': 'forwards'
					});
					break;

				case 4 :
					$('div p:nth-child(1)').css({
						'visibility':'visible',
						'-webkit-animation': 'cssAnimationD '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-moz-animation': 'cssAnimationD '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-o-animation': 'cssAnimationD '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-webkit-animation-fill-mode': 'forwards',
						'-moz-animation-fill-mode': 'forwards',
						'-o-animation-fill-mode': 'forwards'
					});
					break;

				case 3 :
					$('div p:nth-child(1)').css({
						'visibility':'visible',
						'-webkit-animation': 'cssAnimationC '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-moz-animation': 'cssAnimationC '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-o-animation': 'cssAnimationC '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-webkit-animation-fill-mode': 'forwards',
						'-moz-animation-fill-mode': 'forwards',
						'-o-animation-fill-mode': 'forwards'
					});
					break;

				case 2 :
					$('div p:nth-child(1)').css({
						'visibility':'visible',
						'-webkit-animation': 'cssAnimationB '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-moz-animation': 'cssAnimationB '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-o-animation': 'cssAnimationB '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-webkit-animation-fill-mode': 'forwards',
						'-moz-animation-fill-mode': 'forwards',
						'-o-animation-fill-mode': 'forwards'
					});
					break;

				case 1 :
					$('div p:nth-child(1)').css({
						'visibility':'visible',
						'-webkit-animation': 'cssAnimationA '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-moz-animation': 'cssAnimationA '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-o-animation': 'cssAnimationA '+(audioSource.buffer.duration+1)+'s 1 linear',
						'-webkit-animation-fill-mode': 'forwards',
						'-moz-animation-fill-mode': 'forwards',
						'-o-animation-fill-mode': 'forwards'
					});
					break;
			}
				audioSource.start(1);
		});
	}



/*

	$(".tlt").textillate({
		loop: true,
		initialDelay: 500,
		in: {
			effect: "wobble"
		},
		out: {
			effect: "flip"
		},
	});
	$(".segment1").textillate({
		in: {
			effect: "fadeInLeftBig", shuffle: true
		},
	});
	$(".emphasis1").textillate({
		in: {
			effect: "fadeInLeftBig", delay: 180, shuffle: true
		},
	});
	$(".segment2left").textillate({
		in: {
			effect: "rotateInDownLeft"
		},
	});
	$(".segment2right").textillate({
		initialDelay: 500,
		in: {
			effect: "rotateInDownRight"
		}
	});
	$(".segment3left").textillate({
		in: {
			effect: "bounceInLeft", delay: 30, reverse: true,
		}
	});
	$(".segment3right").textillate({
		in: {
			effect: "bounceInRight", delay: 30, reverse: true,
		}
	});
	$(".segment4").textillate({
		in: {
			effect: "flip", delay: 40
		}
	});
	$(".segment4extend").textillate({
		initialDelay: 2000,
		in: {
			effect: "flip", delay: 40
		}
	});
	$(".segment5").textillate({
		in: {
			effect: "bounceIn", shuffle: true
		}
	});
	$(".emphasis5").textillate({
		in: {
			effect: "bounceIn", delay: 280, shuffle: true
		}
	});
	$(".segment6top").textillate({
		in: {
			effect: "fadeInRightBig", delay: 30
		}
	});
	$(".segment6middle").textillate({
		initialDelay: 500,
		in: {
			effect: "fadeInRightBig", delay: 30
		}
	});
	$(".segment6bottom").textillate({
		initialDelay: 1000,
		in: {
			effect: "fadeInRightBig", delay: 30
		}
	});
	$(".segment7_1").textillate({
		in: {
			effect: "fadeInLeftBig", sync: true
		}
	});
	$(".segment7_2").textillate({
		initialDelay: 200,
		in: {
			effect: "fadeInLeftBig", sync: true
		}
	});
	$(".segment7_3").textillate({
		initialDelay: 400,
		in: {
			effect: "fadeInLeftBig", sync: true
		}
	});
	$(".segment7_4").textillate({
		initialDelay: 600,
		in: {
			effect: "fadeInLeftBig", sync: true
		}
	});
	$(".segment7_5").textillate({
		initialDelay: 800,
		in: {
			effect: "fadeInLeftBig", sync: true
		}
	});
	$(".emphasis7").textillate({
		initialDelay: 800,
		in: {
			effect: "fadeInLeftBig", sync: true
		}
	})
	$(".segment8").textillate({
		in: {
			effect: "fadeInUpBig", shuffle: true
		}
	})

*/


});