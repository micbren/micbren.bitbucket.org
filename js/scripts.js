$(document).ready(function(){


	$("div p:nth-child(1)").textillate({
		initialDelay: $(this).data('initial-delay'),
		in: {
			delay: $('div p:nth-child(1)').data('delay'),
			effect: $('div p:nth-child(1)').data('effect'),
			shuffle: $('div p:nth-child(1)').data('shuffle'),
			sync: $('div p:nth-child(1)').data('sync'),
			reverse: $('div p:nth-child(1)').data('reverse'),
		},
	});
	$('div p:nth-child(1)').on('inAnimationBegin.tlt', function () {
  		$(this).css({'visibility':'visible'});
	});


	$("div p:nth-child(2)").textillate({
		initialDelay: $('div p:nth-child(2)').data('initial-delay'),
		in: {
			delay: $('div p:nth-child(2)').data('delay'),
			effect: $('div p:nth-child(2)').data('effect'),
			shuffle: $('div p:nth-child(2)').data('shuffle'),
			sync: $('div p:nth-child(2)').data('sync'),
			reverse: $('div p:nth-child(2)').data('reverse'),
		},
	});
	$('div p:nth-child(2)').on('inAnimationBegin.tlt', function () {
  		$(this).css({'visibility':'visible'});
	});



	$("div p:nth-child(3)").textillate({
		initialDelay: $('div p:nth-child(3)').data('initial-delay'),
		in: {
			delay: $('div p:nth-child(3)').data('delay'),
			effect: $('div p:nth-child(3)').data('effect'),
			shuffle: $('div p:nth-child(3)').data('shuffle'),
			sync: $('div p:nth-child(3)').data('sync'),
			reverse: $('div p:nth-child(3)').data('reverse'),
		},
	});
	$('div p:nth-child(3)').on('inAnimationBegin.tlt', function () {
  		$(this).css({'visibility':'visible'});
	});	


		$("div p:nth-child(4)").textillate({
		initialDelay: $('div p:nth-child(4)').data('initial-delay'),
		in: {
			delay: $('div p:nth-child(4)').data('delay'),
			effect: $('div p:nth-child(4)').data('effect'),
			shuffle: $('div p:nth-child(4)').data('shuffle'),
			sync: $('div p:nth-child(4)').data('sync'),
			reverse: $('div p:nth-child(4)').data('reverse'),
		},
	});
	$('div p:nth-child(4)').on('inAnimationBegin.tlt', function () {
  		$(this).css({'visibility':'visible'});
	});	


	$("div p:nth-child(5)").textillate({
		initialDelay: $('div p:nth-child(5)').data('initial-delay'),
		in: {
			delay: $('div p:nth-child(5)').data('delay'),
			effect: $('div p:nth-child(5)').data('effect'),
			shuffle: $('div p:nth-child(5)').data('shuffle'),
			sync: $('div p:nth-child(5)').data('sync'),
			reverse: $('div p:nth-child(5)').data('reverse'),
		},
	});
	$('div p:nth-child(5)').on('inAnimationBegin.tlt', function () {
  		$(this).css({'visibility':'visible'});
	});	



/*

	$(".tlt").textillate({
		loop: true,
		initialDelay: 500,
		in: {
			effect: "wobble"
		},
		out: {
			effect: "flip"
		},
	});
	$(".segment1").textillate({
		in: {
			effect: "fadeInLeftBig", shuffle: true
		},
	});
	$(".emphasis1").textillate({
		in: {
			effect: "fadeInLeftBig", delay: 180, shuffle: true
		},
	});
	$(".segment2left").textillate({
		in: {
			effect: "rotateInDownLeft"
		},
	});
	$(".segment2right").textillate({
		initialDelay: 500,
		in: {
			effect: "rotateInDownRight"
		}
	});
	$(".segment3left").textillate({
		in: {
			effect: "bounceInLeft", delay: 30, reverse: true,
		}
	});
	$(".segment3right").textillate({
		in: {
			effect: "bounceInRight", delay: 30, reverse: true,
		}
	});
	$(".segment4").textillate({
		in: {
			effect: "flip", delay: 40
		}
	});
	$(".segment4extend").textillate({
		initialDelay: 2000,
		in: {
			effect: "flip", delay: 40
		}
	});
	$(".segment5").textillate({
		in: {
			effect: "bounceIn", shuffle: true
		}
	});
	$(".emphasis5").textillate({
		in: {
			effect: "bounceIn", delay: 280, shuffle: true
		}
	});
	$(".segment6top").textillate({
		in: {
			effect: "fadeInRightBig", delay: 30
		}
	});
	$(".segment6middle").textillate({
		initialDelay: 500,
		in: {
			effect: "fadeInRightBig", delay: 30
		}
	});
	$(".segment6bottom").textillate({
		initialDelay: 1000,
		in: {
			effect: "fadeInRightBig", delay: 30
		}
	});
	$(".segment7_1").textillate({
		in: {
			effect: "fadeInLeftBig", sync: true
		}
	});
	$(".segment7_2").textillate({
		initialDelay: 200,
		in: {
			effect: "fadeInLeftBig", sync: true
		}
	});
	$(".segment7_3").textillate({
		initialDelay: 400,
		in: {
			effect: "fadeInLeftBig", sync: true
		}
	});
	$(".segment7_4").textillate({
		initialDelay: 600,
		in: {
			effect: "fadeInLeftBig", sync: true
		}
	});
	$(".segment7_5").textillate({
		initialDelay: 800,
		in: {
			effect: "fadeInLeftBig", sync: true
		}
	});
	$(".emphasis7").textillate({
		initialDelay: 800,
		in: {
			effect: "fadeInLeftBig", sync: true
		}
	})
	$(".segment8").textillate({
		in: {
			effect: "fadeInUpBig", shuffle: true
		}
	})

*/


});